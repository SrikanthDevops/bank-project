﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Bank.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Formerly Lehman Brothers";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        public ActionResult Clients()
        {
            ViewBag.Message = "Your contact page.";

            return RedirectToRoute("Clients");
        }

        public ActionResult Accounts()
        {
               return View();
        }

        public ActionResult Transfers()
        {

            return View();
        }
    }
}