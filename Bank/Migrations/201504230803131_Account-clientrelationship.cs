namespace Bank.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Accountclientrelationship : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Accounts", "ClientID", "dbo.Clients");
            DropIndex("dbo.Accounts", new[] { "ClientID" });
            AlterColumn("dbo.Accounts", "ClientID", c => c.Int());
            CreateIndex("dbo.Accounts", "ClientID");
            AddForeignKey("dbo.Accounts", "ClientID", "dbo.Clients", "ID");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Accounts", "ClientID", "dbo.Clients");
            DropIndex("dbo.Accounts", new[] { "ClientID" });
            AlterColumn("dbo.Accounts", "ClientID", c => c.Int(nullable: false));
            CreateIndex("dbo.Accounts", "ClientID");
            AddForeignKey("dbo.Accounts", "ClientID", "dbo.Clients", "ID", cascadeDelete: true);
        }
    }
}
