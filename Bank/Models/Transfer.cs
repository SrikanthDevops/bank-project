﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Bank.Models
{
    public class Transfer
    {
        public int ID { get; set; }

        [ForeignKey("Source")]
        public virtual int? SourceID { get; set; }
        public virtual Account Source { get; set; }

        [ForeignKey("Target")]
        public virtual int? TargetID { get; set; }
        public virtual Account Target { get; set; }

        [Required()]
        public decimal Value { get; set; }

        [Required()]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime Date { get; set; }

        public string Details { get; set; }


    }
}